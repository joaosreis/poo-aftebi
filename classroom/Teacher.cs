﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    class Teacher : Person
    {
        private string Field;

        public Teacher(string Name, int Age, string Field)
        {
            this.Name = Name;
            this.Age = Age;
            this.Field = Field;
        }

        public override void Work()
        {
            Console.WriteLine(Name + " is lecturing on " + Field);
        }

        public void SetName(string Name)
        {
            this.Name = Name;
        }

        public string GetName()
        {
            return Name;
        }

        public int GetAge()
        {
            return Age;
        }

        public void SetAge(int Age)
        {
            this.Age = Age;
        }

        public string GetField()
        {
            return Field;
        }

        public void SetField(string Field)
        {
            this.Field = Field;
        }

        public override string ToString()
        {
            string s = base.ToString();
            s += "\nField: " + Field;
            s += "\n-----------------------";
            return s;
        }
    }
}
