﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    class Student : Person
    {
        private float GPA;

        public Student(string Name, int Age, float GPA)
        {
            this.Name = Name;
            this.Age = Age;
            this.GPA = GPA;
        }
        public Student()
        {
            // left blank intentionally
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return Name;
        }

        public int GetAge()
        {
            return Age;
        }

        public void SetAge(int Age)
        {
            this.Age = Age;
        }

        public float GetGPA()
        {
            return GPA;
        }

        public void SetGPA(float GPA)
        {
            this.GPA = GPA;
        }

        public override void Work()
        {
            Console.WriteLine(Name + " is studying");
        }

        public override string ToString()
        {
            string s = base.ToString();
            s += "\nGPA: " + GPA;
            s += "\n-----------------------";
            return s;
        }
    }
}
