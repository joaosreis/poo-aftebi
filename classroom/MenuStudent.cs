using System;
using System.Collections.Generic;

namespace Classroom
{
    class MenuStudent
    {
        private List<Student> students;

        private void criarAluno()
        {
            Console.WriteLine("Introduza o nome do aluno:");
            string name = Console.ReadLine();
            bool readSuccessfully;
            int age;
            float gpa;
            do
            {
                Console.WriteLine("Introduza a idade do aluno:");
                /*
                 * lemos a uma linha inteira do teclado (Console.ReadLine())
                 * tentamos ler um inteiro nessa linha (TryParse)
                 *  em caso de sucesso o "booleano" success fica com o valor true
                 *  e o valor lido fica guardado na variavel age
                */
                readSuccessfully = int.TryParse(Console.ReadLine(), out age);
            } while (!readSuccessfully);
            Console.WriteLine("Introduza a media do aluno:");
            do
            {
                /*
                 * lemos a uma linha inteira do teclado (Console.ReadLine())
                 * tentamos ler um decimal nessa linha (TryParse)
                 *  em caso de sucesso o "booleano" success fica com o valor true
                 *  e o valor lido fica guardado na variavel gpa
                */
                readSuccessfully = float.TryParse(Console.ReadLine(), out gpa);
            } while (!readSuccessfully);
            // instanciamos o estudante
            Student s = new Student(name, age, gpa);
            // adicionamos o estudante à lista de estudantes
            students.Add(s);
        }

        private void listarAlunos()
        {
            /*
                Para cada aluno 'x' presente na lista de alunos
                invocar o método Console.WriteLine(x)
                (basicamente é escrever cada aluno para a consola)
            */
            students.ForEach(x => Console.WriteLine(x));
        }

        private void editarAluno()
        {
            // Pedimos o nome do aluno a pesquisar
            Console.Write("Introduza o nome do aluno a editar: ");
            string name = Console.ReadLine();
            // Pesquisámos a lista pelo primeiro aluno cujo nome contenha o valor
            // inserido pelo utilizador
            Student s = students.Find(s => s.GetName().Contains(name));
            // O resultado do Find é null se não encontrar nada
            if (s == null)
            {
                Console.WriteLine("Aluno não encontrado.");
            }
            else
            {
                // Imprimimos o aluno encontrado
                Console.WriteLine(s);
                // Pedimos ao utilizador os novos dados do aluno
                Console.Write("Introduza o nome: ");
                name = Console.ReadLine();
                Console.Write("Introduza a idade: ");
                int.TryParse(Console.ReadLine(), out int age);
                Console.Write("Introduza média: ");
                float.TryParse(Console.ReadLine(), out float gpa);
                // Usámos os setters para atualizar os dados do aluno
                s.SetName(name);
                s.SetAge(age);
                s.SetGPA(gpa);
            }
        }

        private void eliminarAluno()
        {
            // Pedimos o nome do aluno a pesquisar
            Console.Write("Introduza o nome do aluno a eliminar: ");
            string name = Console.ReadLine();
            // Pesquisámos a lista pelo primeiro aluno cujo nome contenha o valor
            // inserido pelo utilizador
            Student s = students.Find(s => s.GetName().Contains(name));
            // O resultado do Find é null se não encontrar nada
            if (s == null)
            {
                Console.WriteLine("Aluno não encontrado.");
            }
            else
            {
                // Imprimir o aluno encontrado
                Console.WriteLine(s);
                Console.Write("Tem a certeza que quer apagar? ");
                string confirmacao = Console.ReadLine();
                if (confirmacao.Equals("S"))
                {
                    // Remove a primeira ocorrência de s na lista
                    students.Remove(s);
                }
            }
        }

        public MenuStudent(List<Student> students)
        {
            this.students = students;
        }

        public void menu()
        {
            while (true)
            {
                bool success;
                int option;
                do
                {
                    Console.WriteLine("Introduza a opção desejada:");
                    Console.WriteLine("1 -> Criar Aluno");
                    Console.WriteLine("2 -> Listar Alunos");
                    Console.WriteLine("3 -> Editar aluno");
                    Console.WriteLine("4 -> Remover aluno");
                    Console.WriteLine("\n\n0 -> Voltar atrás");
                    /*
                     * lemos a uma linha inteira do teclado (Console.ReadLine())
                     * tentamos ler um inteiro nessa linha (TryParse)
                     *  em caso de sucesso o "booleano" success fica com o valor true
                     *  e o valor lido fica guardado na variavel option
                     */
                    success = int.TryParse(Console.ReadLine(), out option);
                } while (!success);

                switch (option)
                {
                    case 1:
                        criarAluno();
                        break;
                    case 2:
                        listarAlunos();
                        break;
                    case 3:
                        editarAluno();
                        break;
                    case 4:
                        eliminarAluno();
                        break;
                    case 0:
                        return;
                    default:
                        Console.WriteLine("Opção Inválida!");
                        break;
                }
            }


        }
    }
}
