using System;
using System.Collections.Generic;

namespace Classroom
{
    class MenuCourse
    {
        private List<Student> students;
        private List<Teacher> teachers;
        private List<Course> Classroom;

        public MenuCourse(List<Student> students, List<Teacher> teachers, List<Course> Classroom)
        {
            this.students = students;
            this.teachers = teachers;
            this.Classroom = Classroom;
        }

        private void criarDisciplina()
        {
            // verificar se existe pelo menos um professor
            if (teachers.Count == 0)
            {
                Console.WriteLine("Não é permitdo criar disciplina sem existirem professores");
                return;
            }
            Console.WriteLine("Introduza o nome da disciplina:");
            string name = Console.ReadLine();
            bool readSuccessfully;
            do
            {
                Console.WriteLine("Introduza o nome do professor:");
                // estamos a usar a variavel field para o nome temporario do professor a procurar
                string field = Console.ReadLine();
                readSuccessfully = false;
                foreach (Teacher te in teachers)
                {
                    if (te.GetName().Equals(field))
                    {
                        // instanciamos a nova disciplina e adicionamos à lista de disciplinas
                        Classroom.Add(new Course(name, te));
                        readSuccessfully = true;
                    }
                }
            } while (!readSuccessfully);
        }
        private void listarDisciplinas()
        {
            /*
                Para cada disciplina 'x' presente na lista de 
                disicplinas
                invocar o método Console.WriteLine(x)
                (basicamente é escrever cada disicplina para a consola)
            */
            Classroom.ForEach(x => Console.WriteLine(x));
        }

        private void adicionarAlunoADisciplina()
        {
            // verificar se existem alunos
            if (students.Count == 0)
            {
                Console.WriteLine("Não existem alunos para adicionar a disciplinas");
                return;
            }
            // verificar se existem disciplinas
            if (Classroom.Count == 0)
            {
                Console.WriteLine("Não existem disciplinas");
                return;
            }

            bool readSuccessfully = false;
            do
            {
                // listar disciplinas disponiveis
                Console.WriteLine("---- Discipinas Disponiveis ----");
                Classroom.ForEach(x => Console.WriteLine(x.GetName()));
                Console.WriteLine("Introduza o nome da disciplina onde pretende adicionar o aluno");
                string name = Console.ReadLine();

                // procurar a disciplina por nome
                foreach (Course cs in Classroom)
                {
                    if (cs.GetName().Equals(name))
                    {
                        Student s = new Student();
                        do
                        {
                            // listar alunos disponiveis
                            Console.WriteLine("---- Alunos Disponiveis ----");
                            students.ForEach(x => Console.WriteLine(x.GetName()));
                            Console.WriteLine("Introduza o nome do aluno a adicionar");
                            name = Console.ReadLine();
                            readSuccessfully = false;
                            // procurar o aluno por nome
                            foreach (Student st in students)
                            {
                                if (st.GetName().Equals(name))
                                {
                                    s = st;
                                    readSuccessfully = true;
                                }
                            }
                        } while (!readSuccessfully);
                        // adicionar o aluno à disiciplina
                        cs.AddStudent(s);
                        readSuccessfully = true;
                    }
                }
            } while (!readSuccessfully);
        }

        private void adicionarMensagemADisciplina()
        {
            // verificar se ha disicplinas                        
            if (Classroom.Count == 0)
            {
                Console.WriteLine("Não existem disciplinas");
                return;
            }
            // verificar se ha alunos
            if (students.Count == 0)
            {
                Console.WriteLine("Não existem alunos");
                return;
            }

            bool readSuccessfully = false;
            do
            {
                // listar disciplinas disponiveis
                Console.WriteLine("---- Discipinas Disponiveis ----");
                Classroom.ForEach(x => Console.WriteLine(x.GetName()));
                Console.WriteLine("Introduza o nome da disciplina onde pretende adicionar a mensagem");
                string name = Console.ReadLine();
                // procurar disciplina por nome
                foreach (Course cs in Classroom)
                {
                    if (cs.GetName().Equals(name))
                    {
                        Message m = new Message();
                        do
                        {
                            /*
                             * Listar alunos disponiveis
                             * ATENÇÃO: neste momento so os alunos é que podem adicionar mensagens à disciplina
                             * TODO: adicionar funcionalidade para os profs
                             * 
                             */
                            Console.WriteLine("---- Alunos Disponiveis ----");
                            students.ForEach(x => Console.WriteLine(x.GetName()));
                            Console.WriteLine("Introduza o nome do aluno a adicionar que escrevera a mensagem");
                            name = Console.ReadLine();
                            readSuccessfully = false;
                            // procurar aluno por nome
                            foreach (Student st in students)
                            {
                                if (st.GetName().Equals(name))
                                {
                                    // invocar o metodo de criação de mensagem
                                    m = st.PostMessage();
                                    readSuccessfully = true;
                                }
                            }
                        } while (!readSuccessfully);
                        // adicionar mensage à disciplina
                        cs.AddMessage(m);
                        readSuccessfully = true;
                    }
                }
            } while (!readSuccessfully);
        }

        private void listarDisciplina()
        {
            bool readSuccessfully = false;
            do
            {
                // listar disciplinas disponiveis
                Console.WriteLine("---- Discipinas Disponiveis ----");
                Classroom.ForEach(x => Console.WriteLine(x.GetName()));
                Console.WriteLine("Introduza o nome da disciplina que pretende ver integralmente");
                string name = Console.ReadLine();
                foreach (Course ct in Classroom)
                {
                    if (ct.GetName().Equals(name))
                    {
                        Console.WriteLine(ct);
                    }
                }


            } while (!readSuccessfully);
        }

        private void editarDisciplina()
        {
            // Pedimos o nome da disciplina a pesquisar
            Console.Write("Introduza o nome da disciplina a editar: ");
            string name = Console.ReadLine();
            // Pesquisámos a lista pela primeira disciplina cujo nome contenha o valor
            // inserido pelo utilizador
            Course c = Classroom.Find(x => x.GetName().Contains(name));
            // O resultado do Find é null se não encontrar nada
            if (c == null)
            {
                Console.WriteLine("Disciplina não encontrada.");
            }
            else
            {
                // Imprimimos a disciplina encontrado
                Console.WriteLine(c);
                // Pedimos ao utilizador os novos dados da disciplina
                Console.Write("Introduza o nome: ");
                name = Console.ReadLine();
                // Usámos os setters para atualizar os dados da disciplina
                c.SetName(name);
            }
        }

        private void eliminarDisciplina()
        {
            // Pedimos o nome da disciplina a pesquisar
            Console.Write("Introduza o nome da disciplina a eliminar: ");
            string name = Console.ReadLine();
            // Pesquisámos a lista pela primeira disciplina cujo nome contenha o valor
            // inserido pelo utilizador
            Course c = Classroom.Find(x => x.GetName().Contains(name));
            // O resultado do Find é null se não encontrar nada
            if (c == null)
            {
                Console.WriteLine("Disciplina não encontrada.");
            }
            else
            {
                // Imprimir a disciplina encontrado
                Console.WriteLine(c);
                Console.Write("Tem a certeza que quer apagar? ");
                string confirmacao = Console.ReadLine();
                if (confirmacao.Equals("S"))
                {
                    // Remove a primeira ocorrência de s na lista
                    Classroom.Remove(c);
                }
            }
        }

        public void menu()
        {
            while (true)
            {
                bool success;
                int option;
                do
                {
                    Console.WriteLine("Introduza a opção desejada:");
                    Console.WriteLine("1 -> Criar Disciplina");
                    Console.WriteLine("2 -> Listar Disciplinas");
                    Console.WriteLine("3 -> Adicionar aluno a disciplina");
                    Console.WriteLine("4 -> Adicionar mensagem a disciplina");
                    Console.WriteLine("5 -> Listar uma disciplina");
                    Console.WriteLine("6 -> Editar disciplina");
                    Console.WriteLine("7 -> Remover disciplina");
                    Console.WriteLine("\n\n0 -> Voltar atrás");
                    /*
                     * lemos a uma linha inteira do teclado (Console.ReadLine())
                     * tentamos ler um inteiro nessa linha (TryParse)
                     *  em caso de sucesso o "booleano" success fica com o valor true
                     *  e o valor lido fica guardado na variavel option
                     */
                    success = int.TryParse(Console.ReadLine(), out option);
                } while (!success);

                switch (option)
                {
                    case 1:
                        criarDisciplina();
                        break;
                    case 2:
                        listarDisciplinas();
                        break;
                    case 3:
                        adicionarAlunoADisciplina();
                        break;
                    case 4:
                        adicionarMensagemADisciplina();
                        break;
                    case 5:
                        listarDisciplina();
                        break;
                    case 6:
                        editarDisciplina();
                        break;
                    case 7:
                        eliminarDisciplina();
                        break;
                    case 0:
                        return;
                    default:
                        Console.WriteLine("Opção Inválida!");
                        break;
                }
            }
        }
    }
}
