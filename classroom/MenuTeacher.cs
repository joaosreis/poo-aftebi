using System;
using System.Collections.Generic;

namespace Classroom
{
    class MenuTeacher
    {
        private List<Teacher> teachers;

        private void criarProfessor()
        {
            Console.WriteLine("Introduza o nome do professor:");
            string name = Console.ReadLine();
            bool readSuccessfully;
            int age;
            do
            {
                Console.WriteLine("Introduza a idade do professor:");
                /*
                 * lemos a uma linha inteira do teclado (Console.ReadLine())
                 * tentamos ler um inteiro nessa linha (TryParse)
                 *  em caso de sucesso o "booleano" success fica com o valor true
                 *  e o valor lido fica guardado na variavel age
                */
                readSuccessfully = int.TryParse(Console.ReadLine(), out age);
            } while (!readSuccessfully);
            Console.WriteLine("Introduza a area do professor:");
            string field = Console.ReadLine();
            // instancioamos um novo professor
            Teacher t = new Teacher(name, age, field);
            // adicionamos o professor à lista de professores
            teachers.Add(t);
        }

        private void listarProfessores()
        {
            /*
                         * Para cada professor 'x' presente na lista de 
                         * professores
                         * invocar o método Console.WriteLine(x)
                         *  ( basicamente é escrever cada prof para a consola)
                         */
            teachers.ForEach(x => Console.WriteLine(x));
        }

        private void editarProfessor()
        {
            // Pedimos o nome do professor a pesquisar
            Console.Write("Introduza o nome do professor a editar: ");
            string name = Console.ReadLine();
            // Pesquisámos a lista pelo primeiro professor cujo nome contenha o valor
            // inserido pelo utilizador
            Teacher t = teachers.Find(x => x.GetName().Contains(name));
            // O resultado do Find é null se não encontrar nada
            if (t == null)
            {
                Console.WriteLine("Professor não encontrado.");
            }
            else
            {
                // Imprimimos o professor encontrado
                Console.WriteLine(t);
                // Pedimos ao utilizador os novos dados do professor
                Console.Write("Introduza o nome: ");
                name = Console.ReadLine();
                Console.Write("Introduza a idade: ");
                int.TryParse(Console.ReadLine(), out int age);
                Console.Write("Introduza a área de estudo: ");
                string field = Console.ReadLine();
                // Usámos os setters para atualizar os dados do professor
                t.SetName(name);
                t.SetAge(age);
                t.SetField(field);
            }
        }

        private void eliminarProfessor()
        {
            // Pedimos o nome do professor a pesquisar
            Console.Write("Introduza o nome do professor a eliminar: ");
            string name = Console.ReadLine();
            // Pesquisámos a lista pelo primeiro professor cujo nome contenha o valor
            // inserido pelo utilizador
            Teacher t = teachers.Find(x => x.GetName().Contains(name));
            // O resultado do Find é null se não encontrar nada
            if (t == null)
            {
                Console.WriteLine("Professor não encontrado.");
            }
            else
            {
                // Imprimir o professor encontrado
                Console.WriteLine(t);
                Console.Write("Tem a certeza que quer apagar? ");
                string confirmacao = Console.ReadLine();
                if (confirmacao.Equals("S"))
                {
                    // Remove a primeira ocorrência de s na lista
                    teachers.Remove(t);
                }
            }
        }

        public MenuTeacher(List<Teacher> teachers)
        {
            this.teachers = teachers;
        }

        public void menu()
        {
            while (true)
            {
                bool success;
                int option;
                do
                {
                    Console.WriteLine("Introduza a opção desejada:");
                    Console.WriteLine("1 -> Criar Professor");
                    Console.WriteLine("2 -> Listar Professores");
                    Console.WriteLine("3 -> Editar professor");
                    Console.WriteLine("4 -> Remover professor");
                    Console.WriteLine("\n\n0 -> Voltar atrás");
                    /*
                     * lemos a uma linha inteira do teclado (Console.ReadLine())
                     * tentamos ler um inteiro nessa linha (TryParse)
                     *  em caso de sucesso o "booleano" success fica com o valor true
                     *  e o valor lido fica guardado na variavel option
                     */
                    success = int.TryParse(Console.ReadLine(), out option);
                } while (!success);

                switch (option)
                {
                    case 1:
                        criarProfessor();
                        break;
                    case 2:
                        listarProfessores();
                        break;
                    case 3:
                        editarProfessor();
                        break;
                    case 4:
                        eliminarProfessor();
                        break;
                    case 0:
                        return;
                    default:
                        Console.WriteLine("Opção Inválida!");
                        break;
                }
            }


        }
    }
}
