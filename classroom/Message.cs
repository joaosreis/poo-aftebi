﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    class Message
    {
        private string Author;
        private string Text;
        private DateTime Date;

        public Message(string Author, string Text)
        {
            this.Author = Author;
            this.Text = Text;
            Date = new DateTime();
            Date = DateTime.Now;
        }

        public Message()
        {
            // left blank on purpose
        }

        public string GetAuthor()
        {
            return Author;
        }

        public void SetAuthor(string Author)
        {
            this.Author = Author;
        }

        public string GetText()
        {
            return Text;
        }

        public void SetText(string Text)
        {
            this.Text = Text;
        }

        public DateTime GetDate()
        {
            return Date;
        }

        public override string ToString()
        {
            string s = "Autor: " + Author + " - " + Date;
            s += "\n" + Text;
            s += "\n--------------------------";
            return s;
        }
    }
}
