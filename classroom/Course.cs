﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    class Course
    {
        private string Name;
        private Teacher Teacher;
        private List<Student> Students;
        private List<Message> Forum;

        public Course(string Name, Teacher Teacher)
        {
            this.Name = Name;
            this.Teacher = Teacher;
            Students = new List<Student>();
            Forum = new List<Message>();
        }

        public string GetName()
        {
            return Name;
        }

        public void SetName(string Name)
        {
            this.Name = Name;
        }

        public void SetTeacher(Teacher Teacher)
        {
            this.Teacher = Teacher;
        }

        public Teacher GetTeacher()
        {
            return Teacher;
        }

        public void AddStudent(Student s)
        {
            Students.Add(s);
        }

        public void AddMessage(Message m)
        {
            Forum.Add(m);
        }

        public override string ToString()
        {
            string s = "Nome: " + Name;
            s += "\nTeacher: " + Teacher.GetName();
            s += "\n---- Students ----\n";
            foreach (Student st in Students)
            {
                s += st + "\n";
            }
            s += "\n------ Forum ------\n";
            foreach (Message m in Forum)
            {
                s += m + "\n";
            }
            return s;
        }


    }
}
