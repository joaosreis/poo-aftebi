﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    abstract class Person
    {
        protected string Name;
        protected int Age;

        public virtual Message PostMessage()
        {
            Console.WriteLine("Introduza a mensage a publicar");
            string text = Console.ReadLine();
            Message m = new Message(Name, text);
            return m;
        }

        public abstract void Work();

        public override string ToString()
        {
            string s = "Name: " + Name;
            s += "\nAge: " + Age;

            return s;
        }
    }
}
