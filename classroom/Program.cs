﻿using System;
using System.Collections.Generic;

namespace Classroom
{
    class Program
    {
        static void Main(string[] args)
        {
            bool success;
            int option;
            List<Course> Classroom = new List<Course>();
            List<Teacher> teachers = new List<Teacher>();
            List<Student> students = new List<Student>();

            MenuStudent menuEstudante = new MenuStudent(students);
            MenuTeacher menuProfessor = new MenuTeacher(teachers);
            MenuCourse menuDisciplina = new MenuCourse(students, teachers, Classroom);

            while (true)
            {
                do
                {
                    Console.WriteLine("Introduza a opção desejada:");
                    Console.WriteLine("1 -> Alunos");
                    Console.WriteLine("2 -> Professores");
                    Console.WriteLine("3 -> Disciplinas");
                    Console.WriteLine("\n\n0 -> Sair");
                    /*
                     * lemos a uma linha inteira do teclado (Console.ReadLine())
                     * tentamos ler um inteiro nessa linha (TryParse)
                     *  em caso de sucesso o "booleano" success fica com o valor true
                     *  e o valor lido fica guardado na variavel option
                     */
                    success = int.TryParse(Console.ReadLine(), out option);
                } while (!success);

                switch (option)
                {
                    case 1:
                        menuEstudante.menu();
                        break;
                    case 2:
                        menuProfessor.menu();
                        break;
                    case 3:
                        menuDisciplina.menu();
                        break;
                    case 0:
                        return;
                    default:
                        Console.WriteLine("Opção Inválida!");
                        break;
                }
            }
        }
    }
}
